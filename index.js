// load module
const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser');
require('dotenv').config()

// PORT
const port = process.env.PORT;

// Call API
const route_api = require('./api')

// Create Instance express
const app = express()

// Call BodyParser
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))


// MySQL
var connection = mysql.createConnection({
    host : process.env.DB_HOST,
    user : process.env.DB_USER,
    password : process.env.DB_PASSWORD,
    database : process.env.DB
});

connection.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});

// Use API
app.use('/api', require('./api'))


app.get('/', (req, res) => {
  try {
    return res.status(200).send("HomePage");
  }catch (error) {
    return res.status(500).send({
      status: 500,
      message: 'Cant Connect'
    });
  }
})

// this matches all routes and all methods
app.use((req, res) => {
 res.status(404).send({
 status: 404,
 error: "Not found"
 })
})

// PORT
app.listen(port, () => {
  console.log(`Start server at port ${port}.`)
})
