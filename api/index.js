const express = require('express')
const router = express.Router()
module.exports = router


router.get('/', (req, res) => {
  return res.status(200).send("Hello API");
})

router.use('/products', require('./product'))
router.use('/order', require('./order'))
