const express = require('express')
const mysql = require('mysql');


// Set router
const router = express.Router()
module.exports = router
router.use(express.json());

// Set MySQL
var connection = mysql.createConnection({
  host : process.env.DB_HOST,
  user : process.env.DB_USER,
  password : process.env.DB_PASSWORD,
  database : process.env.DB
});

// Connect Database
connection.connect(function(err) {
  if (err) throw err;
  console.log("Connected! Product");
});

// --------------------------- Router --------------------------- //

// Get all product
router.get('/',(req,res) => {
  try {
    connection.query('SELECT * FROM product',(err,result) => {

      //  Check if query is empty
      if (result.length < 1) {
        result = 'Not found'
        return res.status(204).send(result)
      }

      return res.status(200).send(result)
    })
  }catch (error) {
    return res.status(500).send({
      status: 500,
      message: 'Cant Connect'
    });
  }
});

// Get product by quantity
router.get('/quantity/:quantity',(req,res) => {
try {

    const  quantity  = Number(req.params.quantity);
    // ****Please Fix Condition check 'undefined' *****

    const gender = req.body.Gender;
    const color = req.body.Color;
    const pattern = req.body.Pattern;
    const size = req.body.Size;

    let sql = 'SELECT * FROM product WHERE '

    // Condition Check Filter
    if (gender != '') {
      sql = sql + "Gender = '" + gender + "'"
    }

    if (color != '') {
      sql = sql + " AND PlainColor = '" + color + "'"
    }

    if (pattern != '') {
      sql = sql + " AND Pattern = '" + pattern + "'"
    }

    if (size != '') {
      sql = sql + " AND Size = '" + size + "'"
    }

    // Set limit each request
    sql = sql + ' LIMIT ?'

    connection.query(sql, [quantity], (err,result) => {

      return res.status(200).send(result)
    })
  }catch (error) {
    return res.status(500).send({
      status: 500,
      message: 'Cant Connect'
    });
  }
});

// Get product each page
router.get('/:page',(req,res) => {
try {
    const  page  = Number(req.params.page);

    // Set default limit and offset
    let limit = 10
    let offset = 0

    // Show product each page
    if (page > 1) {
      offset = limit * (page - 1)
    }

    let sql = 'SELECT * FROM product LIMIT ? OFFSET ?'

    connection.query(sql, [limit, offset], (err,result) => {

      return res.status(200).send(result)
    })
  }catch (error) {
    return res.status(500).send({
      status: 500,
      message: 'Cant Connect'
    });
  }
});


// Get product by filter
router.post('/', (req, res) => {
  try {

    // ****Please Fix Condition check 'undefined' *****

    const gender = req.body.Gender;
    const color = req.body.Color;
    const pattern = req.body.Pattern;
    const size = req.body.Size;

    let sql = 'SELECT * FROM product WHERE '

    // Condition Check Filter
    if (gender != '') {
      sql = sql + "Gender = '" + gender + "'"
    }

    if (color != '') {
      sql = sql + " AND PlainColor = '" + color + "'"
    }

    if (pattern != '') {
      sql = sql + " AND Pattern = '" + pattern + "'"
    }

    if (size != '') {
      sql = sql + " AND Size = '" + size + "'"
    }

    // Query data
    connection.query(sql, (err,result) => {

      //  Check if query is empty
      if (result.length < 1) {
        result = 'Not found'
        return res.status(204).send(result)
      }

      return res.status(200).send(result)
    })

  }catch (error) {
    return res.status(500).send({
      status: 500,
      message: 'Cant Connect'
    });
  }
});
