const express = require('express')
const mysql = require('mysql');


// Set router
const router = express.Router()
module.exports = router
router.use(express.json());

// Set MySQL
var connection = mysql.createConnection({
  host : process.env.DB_HOST,
  user : process.env.DB_USER,
  password : process.env.DB_PASSWORD,
  database : process.env.DB
});

// Connect Database
connection.connect(function(err) {
  if (err) throw err;
  console.log("Connected! Order");
});

router.post('/', (req, res) => {



  const customerID = req.body.CustomerID;
    const productID = req.body.ProductID;
    const totalAmount = req.body.TotalAmount;
    const cusAddress = req.body.Address;
    let dateTime = String(Date.now())

    const post = {
        OrderID: '1',
        CustomerID: customerID,
        ProductID: productID,
        TotalAmount: totalAmount,
        OrderStatus: '1',
        DateOrder: String(Date.now()),
        CusAddress: cusAddress
    }

    console.log(post);

    let sql = "INSERT INTO order VALUES (?,?,?,?,?,?,?);";

    connection.query(sql ,[3, customerID, productID, totalAmount, 1, dateTime, cusAddress],(err) => {
      console.log(err)
 if(err) {
     throw err
 }

    });

});
